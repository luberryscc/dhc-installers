# DHC Installers

Easy install scripts for the  [dhc](https://github.com/jmgao/dhc) input wrapper

# Installation
Download and unzip the [latest release](https://gitlab.com/luberryscc/dhc-installers/-/releases)

Double click on the shortcut for the game you wish to install this to.
