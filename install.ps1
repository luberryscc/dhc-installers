﻿$GAME=$args[0]
$gameOBJ = $(Get-ChildItem HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\* | Get-ItemProperty|Where-Object {$_.DisplayName -match $GAME})
$gameOBJ.InstallLocation

$url = 'https://github.com/jmgao/dhc/releases/latest'
$request = [System.Net.WebRequest]::Create($url)
$response = $request.GetResponse()
$realTagUrl = $response.ResponseUri.OriginalString
$version = $realTagUrl.split('/')[-1]
$version
$fileName = "dhc-$version.zip"
$realDownloadUrl = $realTagUrl.Replace('tag', 'download') + '/' + $fileName
$realDownloadUrl
$zipFile="$env:TEMP\$fileName"
Invoke-WebRequest -UseBasicParsing -Uri $realDownloadUrl -OutFile $zipFile
Expand-Archive -Path $zipFile -DestinationPath $env:TEMP -Force
cp $env:TEMP\dhc\i686\*.dll $gameOBJ.InstallLocation
$lower=$GAME.ToLower()+".toml"
$lower
$install = $gameOBJ.installLocation+"\dhc.toml"
$install
cp $lower -Destination $install
exit